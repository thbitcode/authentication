Authentication
===============

Authentication é um *bitcode* de autenticação/autorização para [ThrustJS](https://gitlab.com/thrustjs/thrust-seed) que utiliza JWT *(JSON Web Token)* como mecanismo principal.

## Tutorial

Supondo que os arquivos *authentication.js* e *jwt.js* estejam no diretório */thrust/app/libs*, edite o arquivo *startup.js* (disponibilizado juntamente com o ThrustJS) e adicione o seguinte conteúdo:

```javascript
var authentication = require("/libs/authentication")
```

Ao iniciar o ThrustJS a variável *authentication* estará disponível no contexto global com as seguintes funções:

```javascript
/**
* Create a valid authentication token and put it into a cookie inside response's header
* @param {Object} params - thrust params object
* @param {Object} request - thrust request object
* @param {Object} response - thrust response object
* @param {Object} userId - user id
* @param {Object} appId - application id (note: an application name could have many application ids)
* @param {Object} data - some additional data that will be available in each request
* @example
* @file login-endpoint.js
* @code authentication.createAuthentication(request, response, 341, 'mobileApp1', {profile: 'admin'})
*/
createAuthentication(params, request, response, userId, appId, data)

/**
* Destroy a valid authentication token if it exists
* @param {Object} params - thrust params object
* @param {Object} request - thrust request object
* @param {Object} response - thrust response object
* @example
* @file logout-endpoint.js 
* @code authentication.destroyAuthentication(request, response)
*/
destroyAuthentication(params, request, response)

/**
* Middleware to be used for authentication and authorization
* @example
* @file startup.js 
* @code http.middlewares.push(securityAuth.validateAccess) //should to be the first middleware to be pushed
*/
validateAccess(params, request, response)

/**
* Set a function to be called to authorize AccessToken to be renoved
* @example
* @file startup.js
* @code 
* authentication.setCanRefreshTokenFn(function(token) {
*  var canRefresh = true //business rule using token param
* })
*/
function setCanRefreshTokenFn(newFn)
```

## Parâmetros de configuração
As propriedades abaixo devem ser configuradas no arquivo *config.json* (distribuído juntamente com o ThrustJS):

``` javascript
"appName": /*String*/,
"jwtKey":  /*String*/,
"notAuthenticatedUrls": /*String or StringArray*/,
"useSecureAuthentication": /*Boolean (Sould to be True in production)*/,
"accessTokenTTL": /*Object*/,
"refreshTokenTTL": /*Object*/
```

Exemplo:

```javascript
/**
@file config.json
*/
{
    "appName": "MyApp",
    "jwtKey": "C@z%C*FTJaNvRgfkXp2s0v8y@B!D(G+O",
    "notAuthenticatedUrls": [
        "/@auth/login",
        "/@auth/activate",
    ],
    "useSecureAuthentication": true,
    "accessTokenTTL": {
        "central": 300000 // 10 minutos
    },
    "refreshTokenTTL": {
        "central": 28800000 // 8 horas
    }
}
```